import logging

from opsdroid.skill import Skill
from opsdroid.matchers import match_regex
from opsdroid.matchers import match_parse

_LOGGER = logging.getLogger(__name__)

import re
import aiohttp
import urllib.parse

version = "v1.0.0"

class MySkill(Skill):
    mention_name = "youtube-dl"

    def __init__(self, opsdroid, config):
        super(MySkill, self).__init__(opsdroid, config)
        # Load custom configuration part
        self.conf = self.opsdroid.config['skills']['youtube-dl']['config']
        if "youtube_dl_url" not in self.conf:
            _LOGGER.error("youtube_dl_url is empty. Set it in the skill config.")

    @match_parse(mention_name, case_sensitive=False, matching_condition='search')
    async def mention(self, message):
        message_lines = message.text.split("\n")
        for line in message_lines:
            matches = re.findall(r"(https?://[^ ]+)", line)
            for url in matches:
                async with aiohttp.ClientSession(trust_env=True) as session:
                    headers = {}
                    data = "url={}".format(urllib.parse.quote_plus(url))
                    auth = None
                    if self.conf["basic_auth"]["username"] != None and self.conf["basic_auth"]["password"] != None:
                        auth = aiohttp.BasicAuth(login=self.conf["basic_auth"]["username"], password=self.conf["basic_auth"]["password"], encoding='utf-8')
                    try:
                        resp = await session.post(self.conf["youtube_dl_url"], data=data, headers=headers, auth=auth)
                    except aiohttp.client_exceptions.ClientConnectorError:
                        await message.respond("Unable to connect youtube-dl service.")
                        return None
                    if resp.status == 200:
                        result = await resp.json()
                        if result["success"] == True:
                            await message.respond("Downloading {} ...".format(url))
                        else:
                            await message.respond("Error downloading {} : {}".format(url, result["error"]))
                    else:
                        result = await resp.text()
                        await message.respond("Error downloading {} : {}".format(url, result))

    @match_regex(r"\s*{}:?\s*".format(mention_name))
    async def help(self, message):
        await message.respond("I'm a youtube-dl bot, I can download videos for you!\nMention me with a youtube URL and I will do the job! ;)")
