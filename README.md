# youtube-dl_bot

## Bot based on [Opsdroid](https://opsdroid.dev) to trigger downloads on [youtube-dl service](https://github.com/ToWatchList/twl-dl-server)

![Matrix](https://img.shields.io/matrix/youtube-dl_bot:fiksel.info?server_fqdn=matrix.fiksel.info)

![screenshot01](images/screenshot01.png)

# Running

## docker-compose

* Clone the repository
```
git clone https://gitlab.com/olegfiksel/youtube-dl_bot.git
cd youtube-dl_bot
```
* Create new config file (copy an example)
```
cp configuration-example.yaml configuration.yaml
```
* Adjust the config `configuration.yaml` to your needs
* Create secrets (copy an example)
```
cp secrets-example.env secrets.env
```
* Start the bot
```
docker-compose up -d
docker-compose logs -f
```
